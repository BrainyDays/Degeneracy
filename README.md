# Degeneracy

Wavelet patterns used in the publication


Dodel S, Tognoli E and Kelso JAS
(2020) Degeneracy and Complexity in
Neuro-Behavioral Correlates of Team
Coordination.
Front. Hum. Neurosci. 14:328.
doi: 10.3389/fnhum.2020.00328

can be found [here](https://drive.google.com/drive/folders/1B0MthmJfaxv530YSugQ7P5cQoWlJ8JWw?usp=sharing) (as MATLAB *.mat files). For reasons of space only the wavelets for one building is uploaded. Access to wavelets from other buildings and to the analysis software can be obtained upon request.

Need more info? Please contact silkedodel@gmail.com